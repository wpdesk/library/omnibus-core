<?php
declare( strict_types=1 );

namespace WPDesk\Omnibus\Core;

use Psr\Log\LoggerAwareTrait;
use WPDesk\Logger\Settings;
use WPDesk\Logger\SimpleLoggerFactory;
use WPDesk\Migrations\WpdbMigrator;
use WPDesk\Omnibus\Core\Migrations\Version100;
use WPDesk\Omnibus\Core\Repository\PriceRepository;
use WPDesk\View\Renderer\SimplePhpRenderer;
use WPDesk\View\Resolver\ChainResolver;
use WPDesk\View\Resolver\DirResolver;
use WPDesk\View\Resolver\WPThemeResolver;

/**
 * Core functionalities are here. You can use them and/or extend.
 * This trait is meant to be used by WPDesk\Builder\AbstractPlugin instance.
 */
trait OmnibusPluginTrait {
	use LoggerAwareTrait;

	public function init(): void {
		WpdbMigrator::from_classes( [ Version100::class ], 'wpdesk_omnibus' )
					->migrate();

		$logger_options             = new Settings();
		$logger_options->use_wp_log = false;
		$this->setLogger( ( new SimpleLoggerFactory( 'wpdesk-omnibus-core', $logger_options ) )->getLogger() );
		parent::init();
	}

	public function hooks(): void {
		global $wpdb;
		$repository = new PriceRepository( $wpdb );

		( new PriceUpdated( $repository, $this->logger ) )->hooks();

		$renderer = new SimplePhpRenderer(
			new ChainResolver(
				new DirResolver( dirname( __DIR__ ) . '/templates/' ),
				new WPThemeResolver( 'wpdesk-omnibus-core' )
			)
		);
		( new DisplayPrice( $repository, $renderer ) )->hooks();
		( new AdminDisplayPrice( $repository ) )->hooks();

		add_action(
			'woocommerce_init',
			function() use ( $repository ): void {
				if ( get_option( PriceMigrator::OMNIBUS_PRICES_MIGRATED, '0' ) === '1' ) {
					return;
				}
				$migrator = new PriceMigrator( $repository, $this->logger );
				$migrator->register( \WC_Queue::instance() );
				$migrator->hooks();
			}
		);
		parent::hooks();
	}

	public function wp_enqueue_scripts() {
		parent::wp_enqueue_scripts();

		if ( ! is_product() ) {
			return;
		}
		if ( property_exists( $this, 'vendor_assets_url' ) ) {
			wp_enqueue_script( 'omnibus', $this->vendor_assets_url . '/index.js', [ 'jquery' ], $this->plugin_info->get_version(), true );
		} else {
			wp_enqueue_script( 'omnibus', $this->plugin_info->get_plugin_url() . '/vendor_prefixed/wpdesk/omnibus-core/assets/index.js', [ 'jquery' ], $this->plugin_info->get_version(), true );
		}
	}

}
