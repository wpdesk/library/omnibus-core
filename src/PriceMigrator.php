<?php
declare( strict_types=1 );

namespace WPDesk\Omnibus\Core;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use WPDesk\Omnibus\Core\Repository\PriceEntity;
use WPDesk\Omnibus\Core\Repository\PriceNotSaved;
use WPDesk\Omnibus\Core\Repository\PriceRepository;
use WPDesk\PluginBuilder\Plugin\Hookable;

class PriceMigrator implements Hookable {
	const OMNIBUS_PRICES_MIGRATED = 'omnibus_prices_migrated';
	const QUEUE_HOOK              = 'wpdesk/omnibus/current_prices_migration';
	const ACTION_QUEUED           = 'omnibus_action_queued';

	/** @var PriceRepository */
	private $repository;

	/** @var LoggerInterface */
	private $logger;

	public function __construct( PriceRepository $repository, ?LoggerInterface $logger = null ) {
		$this->repository = $repository;
		$this->logger     = $logger ?? new NullLogger();
	}

	public function register( \WC_Queue_Interface $queue ): void {
		if ( get_option( self::ACTION_QUEUED, '0' ) === '1' ) {
			return;
		}

		$queue->add( self::QUEUE_HOOK );

		update_option( self::ACTION_QUEUED, '1' );
	}

	public function hooks(): void {
		add_action(
			self::QUEUE_HOOK,
			function(): void {
				$this->save_current_products_prices();
			}
		);
	}

	private function save_current_products_prices(): void {
		$this->logger->info( 'Saving current products information.' );
		foreach ( $this->get_all_products() as $product ) {
			if ( $this->product_already_migrated( $product->get_id() ) ) {
				$this->logger->alert( sprintf( 'Skipping already migrated product %d', $product->get_id() ) );
				continue;
			}

			$price_value             = new PriceEntity(
				$product->get_id(),
				(float) $product->get_regular_price( 'not-for-view' ),
				$product->get_date_modified() ?? $product->get_date_created() ?? new \DateTime()
			);
			$price_value->product_id = 3;

			$this->logger->info( sprintf( 'Saving price value for product %d', $product->get_id() ) );
			try {
				$this->repository->save( $price_value );
				$this->logger->info( sprintf( 'Price value for product %d saved successfully', $product->get_id() ) );
			} catch ( PriceNotSaved $e ) {
				$this->logger->error(
					sprintf(
						'Price value for product %d could not be saved. Reason: %s',
						$product->get_id(),
						$e->getMessage()
					)
				);
			}
		}

		$this->logger->info( 'Current products information saved.' );
		update_option( self::OMNIBUS_PRICES_MIGRATED, '1' );
	}

	/** @return \Generator<\WC_Product> */
	private function get_all_products(): iterable {
		$products = wc_get_products( [ 'limit' => -1 ] );
		if ( ! is_array( $products ) ) {
			return [];
		}

		foreach ( $products as $product ) {
			yield $product;
			if ( $product instanceof \WC_Product_Variable ) {
				// @phpstan-ignore-next-line
				yield from $product->get_available_variations( 'objects' );
			}
		}
	}

	private function product_already_migrated( int $product_id ): bool {
		$price_value = $this->repository->get_cheapest( $product_id );
		return $price_value->price !== 0.0;
	}
}
