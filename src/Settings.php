<?php
declare( strict_types=1 );

namespace WPDesk\Omnibus\Core;

class Settings {

	/**
	 * @var array<string, string|false>
	 */
	private static $cache = [];

	public static function has( string $option ): bool {
		// @phpstan-ignore-next-line
		self::$cache[ $option ] = get_option( 'omnibus_' . $option );
		return ! empty( self::$cache[ $option ] );
	}

	public static function get( string $option ): string {
		if ( ! empty( self::$cache[ $option ] ) ) {
			return self::$cache[ $option ];
		}
		// @phpstan-ignore-next-line
		return self::$cache[ $option ] = (string) get_option( 'omnibus_' . $option ); // phpcs:ignore
	}
}
