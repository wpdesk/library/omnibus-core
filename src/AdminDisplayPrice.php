<?php
declare( strict_types=1 );

namespace WPDesk\Omnibus\Core;

use WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\Omnibus\Core\Repository\PriceRepository;

class AdminDisplayPrice implements Hookable {

	/** @var PriceRepository */
	private $repository;

	public function __construct( PriceRepository $repository ) {
		$this->repository = $repository;
	}

	public function hooks() {
		add_action(
			'woocommerce_product_options_pricing',
			function(): void {
				$this->display_changed_prices();
			}
		);
		add_action(
			'woocommerce_variation_options_pricing',
			function( int $loop, array $variation_data, \WP_Post $variation ): void {
				$this->display_changed_prices_variable( $variation );
			},
			10,
			3
		);
	}

	private function display_changed_prices(): void {
		global $thepostid, $product_object;

		if ( ! is_numeric( $thepostid ) ) {
			return;
		}

		$price_value = $this->repository->get_cheapest( (int) $thepostid );

		woocommerce_wp_text_input(
			[
				'id'                => '_cheapest_price',
				'value'             => $price_value->price ?: $product_object->get_regular_price(),
				'label'             => esc_html__( 'The lowest price', 'wpdesk-omnibus-core' ),
				'data_type'         => 'price',
				'custom_attributes' => [
					'disabled' => 'disabled',
				],
			]
		);

		woocommerce_wp_text_input(
			[
				'id'                => '_cheapest_price_date',
				'value'             => $price_value->created->format( 'Y-m-d' ),
				'label'             => esc_html__( 'The lowest price date', 'wpdesk-omnibus-core' ),
				'custom_attributes' => [
					'disabled' => 'disabled',
				],
			]
		);
	}

	private function display_changed_prices_variable( \WP_Post $variation ): void {
		$price_value    = $this->repository->get_cheapest( $variation->ID );
		$product_object = wc_get_product( $variation->ID );

		woocommerce_wp_text_input(
			[
				'id'                => '_cheapest_price',
				// @phpstan-ignore-next-line
				'value'             => $price_value->price ?: $product_object->get_regular_price(),
				'label'             => esc_html__( 'The lowest price', 'wpdesk-omnibus-core' ),
				'data_type'         => 'price',
				'wrapper_class'     => 'form-row form-row-first',
				'custom_attributes' => [
					'disabled' => 'disabled',
				],
			]
		);

		woocommerce_wp_text_input(
			[
				'id'                => '_cheapest_price_date',
				'value'             => $price_value->created->format( 'Y-m-d' ),
				'label'             => esc_html__( 'The lowest price date', 'wpdesk-omnibus-core' ),
				'wrapper_class'     => 'form-row form-row-last',
				'custom_attributes' => [
					'disabled' => 'disabled',
				],
			]
		);
	}

}
