<?php
declare(strict_types=1);

namespace WPDesk\Omnibus\Core;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use WPDesk\Omnibus\Core\Repository\PriceEntity;
use WPDesk\Omnibus\Core\Repository\PriceNotSaved;
use WPDesk\Omnibus\Core\Repository\PriceRepository;

class PriceUpdated {

	/** @var PriceRepository */
	private $repository;

	/** @var LoggerInterface */
	private $logger;

	public function __construct( PriceRepository $repository, ?LoggerInterface $logger = null ) {
		$this->repository = $repository;
		$this->logger     = $logger ?? new NullLogger();
	}

	public function hooks(): void {
		add_action(
			'woocommerce_after_product_object_save',
			function ( $product ) {
				$this->intercept_price( $product );
			}
		);
	}

	private function intercept_price( \WC_Product $product ): void {
		if ( Settings::get( 'use_sale_price' ) === 'yes' ) {
			$this->save_price_value( $product->get_id(), (float) $product->get_price() );
		} else {
			$this->save_price_value( $product->get_id(), (float) $product->get_regular_price() );
		}
	}

	private function save_price_value( int $product_id, float $price ): void {
		$old_value = $this->repository->get_cheapest( $product_id );
		if ( $old_value->price === $price ) {
			$this->logger->notice( sprintf( 'Price was not changed for %d. Skipping.', $product_id ) );
			return;
		}
		$entity = new PriceEntity( $product_id, $price, new \DateTime() );
		$this->logger->info( sprintf( 'Saving price value for product %d', $entity->product_id ) );
		try {
			$this->repository->save( $entity );
			$this->logger->info( sprintf( 'Price value for product %d successfully saved', $entity->product_id ) );
		} catch ( PriceNotSaved $e ) {
			$this->logger->error( sprintf( 'Price value for product %d could not be saved. Reason: %s', $entity->product_id, $e->getMessage() ) );
		}
	}

}
