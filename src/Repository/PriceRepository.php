<?php
declare(strict_types=1);

namespace WPDesk\Omnibus\Core\Repository;

use WPDesk\Omnibus\Core\Migrations\Schema;
use WPDesk\Omnibus\Core\Settings;

class PriceRepository {
	private const REFERENCE_INTERVAL = 30;

	/** @var \wpdb */
	private $wpdb;

	public function __construct( \wpdb $wpdb ) {
		$this->wpdb = $wpdb;
	}

	public function save( PriceEntity $entity ): int {
		$status = $this->wpdb->insert(
			Schema::price_logger_table_name(),
			[
				'product_id' => $entity->product_id,
				'price'      => $entity->price,
				'created'    => $entity->created->format( 'Y-m-d H:i:s' ),
			]
		);

		if ( $status === false ) {
			throw new PriceNotSaved( $this->wpdb->last_error );
		}

		return $this->wpdb->insert_id;
	}

	public function get_cheapest( int $product_id ): PriceEntity {
		$table    = Schema::price_logger_table_name();
		$id       = esc_sql( (string) $product_id );
		$min_date = ( new \DateTime( $this->get_date_interval() ) )->format( 'Y-m-d H:i:s' );

		// phpcs:disable WordPress.DB.PreparedSQL
		$result = $this->wpdb->get_row(
			"SELECT *
			FROM {$table}
			WHERE `product_id` = {$id}
			AND `created` > '${min_date}'
			ORDER BY `price` ASC
			LIMIT 1",
			ARRAY_A
		);
		// phpcs:enable

		if ( $result !== null ) {
			// @phpstan-ignore-next-line
			return new PriceEntity( $product_id, (float) $result['price'], new \DateTime( $result['created'] ) );
		}

		return new PriceEntity( $product_id, 0.0, new \DateTime() );
	}

	private function get_date_interval(): string {
		if ( Settings::has( 'date_interval' ) ) {
			return '-' . Settings::get( 'date_interval' ) . ' days';
		}
		return '-' . self::REFERENCE_INTERVAL . ' days';
	}

}
