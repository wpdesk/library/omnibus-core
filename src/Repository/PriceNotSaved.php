<?php
declare( strict_types=1 );

namespace WPDesk\Omnibus\Core\Repository;

class PriceNotSaved extends \RuntimeException {}
