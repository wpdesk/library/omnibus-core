<?php
declare(strict_types=1);

namespace WPDesk\Omnibus\Core\Repository;

class PriceEntity {

	/**
	 * @var int
	 * @readonly
	 */
	public $product_id;

	/**
	 * @var float
	 * @readonly
	 */
	public $price;

	/**
	 * @var \DateTimeInterface
	 * @readonly
	 */
	public $created;

	public function __construct(
		int $product_id,
		float $price,
		\DateTimeInterface $created
	) {
		$this->product_id = $product_id;
		$this->price      = $price;
		$this->created    = $created;
	}

}
