<?php
declare(strict_types=1);

namespace WPDesk\Omnibus\Core;

use WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\View\Renderer\Renderer;
use WPDesk\Omnibus\Core\Repository\PriceRepository;
use WPDesk\Omnibus\Core\Repository\PriceEntity;

class DisplayPrice implements Hookable {

	/** @var PriceRepository */
	private $repository;

	/** @var Renderer */
	private $renderer;

	public function __construct( PriceRepository $repository, Renderer $renderer ) {
		$this->repository = $repository;
		$this->renderer   = $renderer;
	}

	public function hooks(): void {
		add_action(
			Settings::has( 'display_hook' ) ?
				Settings::get( 'display_hook' ) :
				'woocommerce_product_meta_start',
			function (): void {
				$this->display_last_min_price();
			}
		);
	}

	private function display_last_min_price(): void {
		global $post;
		$price_value = apply_filters( 'omnibus/core/current_price_value', $this->repository->get_cheapest( $post->ID ) );
		$product     = wc_get_product( $post->ID );
		if ( ! $product instanceof \WC_Product ) {
			return;
		}

		$this->renderer->output_render(
			'display_price',
			[
				'product'            => $product,
				'encoded_variations' => json_encode( $this->get_variations_data( $product->get_id() ) ),
				'price_value'        => $price_value,
				'message'            => $this->get_message_string( $price_value, $product ),
			]
		);
	}

	/**
	 * @param int $parent_id
	 *
	 * @return array<int, array{price: string, date: string}>
	 */
	private function get_variations_data( int $parent_id ): array {
		$product = wc_get_product( $parent_id );
		if ( ! $product instanceof \WC_Product_Variable ) {
			return [];
		}

		$variations_data = [];
		/** @var \WC_Product_Variation $variation */
		foreach ( $product->get_available_variations( 'objects' ) as $variation ) {
			$product_value                           = $this->repository->get_cheapest( $variation->get_id() );
			$variations_data[ $variation->get_id() ] = [
				'price' => wc_price( $product_value->price ?: (float) $variation->get_regular_price() ),
				'date'  => $product_value->created->format( 'Y-m-d' ),
			];
		}

		return $variations_data;
	}

	private function get_message_string( PriceEntity $price_value, \WC_Product $product ): string {
					$raw_message = Settings::has( 'price_message' ) ?
					Settings::get( 'price_message' ) :
					__( 'The lowest price ({date}): {price}', 'wpdesk-omnibus-core' );

					$message = str_ireplace(
						[ '{date}', '{price}' ],
						[
							"<span class='js-omnibus-date'>" . esc_html( $price_value->created->format( 'Y-m-d' ) ) . '</span>',
							"<span class='js-omnibus-price'>" . wc_price( $price_value->price ?: (float) $product->get_regular_price() ) . '</span>',
						],
						$raw_message
					);

					return $message;
	}

}
