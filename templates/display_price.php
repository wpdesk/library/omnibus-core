<?php
/**
 * @var \WC_Product $product
 * @var \WPDesk\Omnibus\Core\Repository\PriceEntity $price_value
 * @var string $encoded_variations
 * @var string $message
 */

?>

<span id='omnibus-price-data' data-variations_data='<?php echo esc_attr( $encoded_variations ); ?>'>
<?php echo wp_kses_post( $message ); ?>
</span>
